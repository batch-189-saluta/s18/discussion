
/*
	Function parameters are variable that wait for a value from an
	argument in the function invocation
*/
function saMyName(name) {
	console.log('My name is ' + name)
}
/* Function arguments are like values like string, numbers, etc.
that can pass onto the function you are invoking*/
saMyName('Slim Shady')

/*you can re-use functions by invokling them at any time or any
pert of your code. Make sure you have declared them first*/
saMyName('Vice Ganda')
saMyName('Joferlyn')

/*You can also pass variables as arguments to a function*/
let myName= 'Earl'
saMyName(myName)


/*You can use arguments and parameters to make the data inside 
your function dynamic*/
function sumOfTwoNumbers(firstNumber, secondNumber) {
	let sum = 0;
	sum = firstNumber + secondNumber
	console.log('The sum of two numbers is ' + sum)
}
sumOfTwoNumbers(10, 15)
sumOfTwoNumbers(99, 1)
sumOfTwoNumbers(50, 20)


/*You can pass a function as an arguement for another function.
Do not pass that function with parentheses, only with its function name*/
function argumentFunction(){
	console.log('This is a function that was passed as an argument.')
}

function parameterFunction(argumentFunction) {
	argumentFunction()
}
parameterFunction(argumentFunction)

/*REAL WORLD APPLICATION*/
/*Imagine a product page where you have an 'add to cart' button
wherein you have the button element itself displayed on the page but 
you dont have the functionality of it yet. This is where passing a
function as an arguement comes in, in order for you to add
or have access to a function to add functionality to your
button once it is clicked*/
function addProductToCart() {
	console.log('Click me to add product to cart')
}
let addToCartBtn = document.querySelector('#add-to-cart-btn')

addToCartBtn.addEventListener('click', addProductToCart)


/*If you add an extra or if you lack an arguement, javascript
wont throw error at you, instead it will ignore the extra arguement
and turn the lacking parameter into undefined*/
function displayFullName(firstName, middleName, lastName, extraText) {
	console.log('Your full name is : ' +firstName+' ' + middleName +' '+ lastName+' '+extraText)
}
displayFullName('francis', 'dc', 'saluta')
displayFullName('Johnny', 'b', 'goode', 'junior')


// STRING INTERPOLATION (use back tick " `a` ")
/*function displayMovieDetails(title, synopsis, director){
	console.log(`The movie titled ${title}`)
	console.log(`The synopsis ${synopsis}`)
	console.log(`The director ${director}`)
}
displayMovieDetails('sisterakas', 'i forgot', 'wenn deramas')*/


/*Return statement/syntax is the final step of any function. It assigns whatever 
value you put after it as the value of the function upon invoking it*/
/*
	Note: 
	1. return  statement should ALWAYS come last in the function scope
	2. Any statement after the return statement will be ignored by the function	
*/
function displayPlayerDetails(name, age, playerClass) {
	
	let playerDetails = `Name: ${name}, Age: ${age}, Class: ${playerClass}`
	return playerDetails
	// dapat laging nasa dulo ang return statement

	console.log('Hello!')
}

console.log(displayPlayerDetails('Elsworth', 120, 'archer')) 
